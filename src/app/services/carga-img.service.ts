import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { FileItem } from '../models/file-item';

import * as firebase from 'firebase';

@Injectable()
export class CargaImgService {

  private CARPETA_IMAGENES:string = 'img';

  constructor(public afdb:AngularFireDatabase) { }


  listaUltimaImagenes(cantidad:number):FirebaseListObservable<any[]>{

    return this.afdb.list(`/${this.CARPETA_IMAGENES}`,{
      query: {
        limitToLast: cantidad
      }
    })

  }

  cargarTodasImg(){
    return this.afdb.list(`/${this.CARPETA_IMAGENES}`)
  }

  cargarImgFb(archivos:FileItem[]){

    console.log(archivos);

    let storageRef = firebase.storage().ref();


    for(let item of archivos){
      item.estado = true;
      let uploadTask:firebase.storage.UploadTask = storageRef.child(`/${this.CARPETA_IMAGENES}/${item.nombreArchivo}`).put(item.archivo);

      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,

          (snapshot) =>  item.progreso = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100,
          (error) => console.error('Error al subir',error),
          ( ):any => {
            item.url = uploadTask.snapshot.downloadURL;
            item.estado = false;
            this.guardarImagen({nombre: item.nombreArchivo, url: item.url});
          }
      )
    }
  }

  private guardarImagen(imagen:any){
    console.log('guardarImagen', imagen);
    this.afdb.list(`/${this.CARPETA_IMAGENES}`).push(imagen);
  }

  eliminarImg( index:string ){

    console.log('eliminando ', index);
    this.afdb.list(`/${this.CARPETA_IMAGENES}`).remove(index);


  }

}
