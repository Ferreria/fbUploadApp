import { Component, OnInit } from '@angular/core';
import { CargaImgService } from '../../services/carga-img.service';
import { FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-fotos',
  templateUrl: './fotos.component.html',
  styles: []
})
export class FotosComponent implements OnInit {

  imagenes:FirebaseListObservable<any[]>;

  constructor(public _cis:CargaImgService) {
    this.imagenes = this._cis.listaUltimaImagenes(5);
   }

  ngOnInit() {
  }


  cargarMasImg(){

  this.imagenes = this._cis.cargarTodasImg();


}
}
