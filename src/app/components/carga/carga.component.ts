import { Component, OnInit } from '@angular/core';
import { FileItem } from '../../models/file-item';
import { CargaImgService } from '../../services/carga-img.service';

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styles: []
})
export class CargaComponent {

  estaSobreDropZone:boolean = false;
  permiteCargar:boolean = true;
  archivos:FileItem[] = [];

  constructor(public _cis:CargaImgService) { }

  archivoSobreDrogZone(evento:boolean){
    this.estaSobreDropZone = evento;
  }

  limpiarArchivos(){
    this.archivos = [];
    this.permiteCargar = true;
  }

  cargarImg(){
    this.permiteCargar = false;
    this._cis.cargarImgFb(this.archivos);
  }

}
